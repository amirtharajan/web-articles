from django.conf import settings
from django.contrib.auth.hashers import make_password

SALT = settings.SALT
HASHER = settings.HASHER

def make_hash(user_password):
    hash_password = make_password(user_password, salt=SALT, hasher=HASHER)
    return hash_password
