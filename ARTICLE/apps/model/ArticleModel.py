from django.db import models

from apps.models import Login



class Article(models.Model):

    login       =   models.ForeignKey(Login, on_delete=models.CASCADE, unique=False)
    title       =   models.CharField(max_length=128, blank=False)
    image       =   models.FileField(upload_to='images/', null=True)
    content     =   models.TextField(null=True)
    created_at  =   models.DateTimeField(auto_now_add=True)
    updated_at  =   models.DateTimeField(null=True)

    objects = models.Manager()


    class Meta:
        db_table = "articles"